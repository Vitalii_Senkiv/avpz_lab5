﻿using AVPZ_Lab5.Infrastructure.Commands.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab5.Infrastructure.Commands
{
    public class LambdaCommand : CommandBase
    {
        Action<object> _execute;
        Func<object, bool> _canExecute;

        public LambdaCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public override bool CanExecute(object parameter) => _canExecute?.Invoke(parameter) ?? true;

        public override void Execute(object parameter) => _execute(parameter);
    }
}
