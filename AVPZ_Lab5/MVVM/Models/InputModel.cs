﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab5.MVVM.Models
{
    public class InputModel
    {
        public string TextDescription { get; set; }
        public double Value { get; set; } = 1;
    }
}
