﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab5.MVVM.Models
{
    public class RiskCostTableRow
    {
        public string Text { get; set; }
        public double Value { get; set; }

        public double Cost { get; set; }

        public double[] Coefficients { get; set; } = new double[20];

        public double ExtraPrice { get; set; }
        public double FinalPrice { get; set; }

        public bool IsBold { get; set; }
    }
}
