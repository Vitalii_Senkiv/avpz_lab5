﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AVPZ_Lab5.MVVM.Models
{
    public class RealisationPriceRow
    {
        public double Sum 
        {
            get => Math.Round(Prices.Sum(), 2);
        }
        public double[] Prices { get; set; } = new double[4];
    }
}
