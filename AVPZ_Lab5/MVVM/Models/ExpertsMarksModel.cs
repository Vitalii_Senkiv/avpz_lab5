﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab5.MVVM.Models
{
    public class ExpertMarksModel
    {
        public int[] ExpertsMark { get; set; }
        public string ExpertArea { get; set; }
        public ExpertMarksModel(int expertsCount)
        {
            ExpertsMark = new int[expertsCount];
        }
    }
}
