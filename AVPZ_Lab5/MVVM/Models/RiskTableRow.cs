﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab5.MVVM.Models
{
    public class RiskTableRow
    {
        public string Text { get; set; }
        public double Value { get; set; }

        public double[] Coefficients { get; set; } = new double[20];
        public double Probability { get; set; }
        public string ProbabilityStr 
        { 
            get
            {
                if (Probability < 0.1)
                {
                    return "Дуже низька";
                }
                else if (Probability < 0.25)
                {
                    return "Низька";
                }
                else if (Probability < 0.5)
                {
                    return "Середня";
                }
                else if (Probability < 0.75)
                {
                    return "Висока";
                }
                else
                {
                    return "Дуже висока";
                }

            }
        }

        public bool IsBold { get; set; }
    }
}
