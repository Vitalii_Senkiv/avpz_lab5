﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AVPZ_Lab5.MVVM.Models
{
    public class Tab4TableModel
    {
        public static double Min { get; set; }
        public static double Max { get; set; }
        public static double MPR { get; set; }

        public string TextDescription { get; set; }
        public double Value { get; set; } = 1;

        public double ExtraPrice { get; set; }
        public string ProbabilityStr
        {
            get
            {
                if (ExtraPrice >= Min && ExtraPrice < Min + MPR)
                {
                    return "Низький";
                }
                else if (ExtraPrice >= Min + MPR && ExtraPrice < Min + 2*MPR)
                {
                    return "Середній";
                }
                else if (ExtraPrice >= Min + 2*MPR && ExtraPrice <= Max)
                {
                    return "Високий";
                }
                else
                {
                    return "-";
                }
            }
        }

        public bool IsBold { get; set; }
    }
}
